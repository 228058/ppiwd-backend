from threading import Thread, Lock
import random
import time


class Sensor:

    def __init__(self, name, gen_freq) -> None:
        self.name = name
        self.gen_freq = gen_freq
        self.samples = []
        self.started = False
        self.lock = Lock()
        self.thread = Thread(target=self.generate_sample, args=(self.gen_freq,))

    def start(self) -> None:
        self.started = True
        self.thread.start()

    def stop(self) -> None:
        self.started = False
        if self.thread.is_alive():
            self.thread.join()

    def generate_sample(self, gen_freq):
        while self.started:
            self.lock.acquire()
            self.samples.append({
                'data_x': random.random() * 100,
                'data_y': random.random() * 100,
                'data_z': random.random() * 100
            })
            self.lock.release()
            time.sleep(1 / gen_freq)

    def get_samples(self):
        self.lock.acquire()
        samples = self.samples
        self.samples = []
        self.lock.release()
        return samples
