from sensor import Sensor
import time
import random
import json


def save_json(dictionary: dict):
    with open("sample.json", "r") as f:
        existing_json = json.load(f)

    merged = existing_json | dictionary
    merged = sorted(merged)
    json_object = json.dumps(merged, indent = 4)
    with open("sample.json", "w") as f:
        f.write(json_object)


def get_data_from(sensor: Sensor, sitting: int, gen_freq: int, include_change: bool = False):
    mls_per_sample = 1000 // gen_freq
    data = sensor.get_samples()
    latest_timestamp = int(time.time() * 1000 + int(random.random() * 1000))
    earliest_timestamp = latest_timestamp - ((len(data) - 1) * mls_per_sample)

    return {
        'data': data,
        'timestamp': latest_timestamp,
        'sensor': sensor.name,
        'sitting': sitting,
        'device_id': 'device_id',
        'change_timestamp': random.randint(earliest_timestamp, latest_timestamp) if include_change else 0
    }


# a function to round to a multiple value like 10 or 5
def round_to_multiple(number: int, multiple: int = 10):
    return int(multiple * round(number / multiple))


def empty_record(sensor_name): 
    return {
        'timestamp': 0,
        'sitting': 0,
        f'{sensor_name}_x': 0,
        f'{sensor_name}_y': 0,
        f'{sensor_name}_z': 0
    }


def receive_data(request_data: dict, acc_data: dict, gyro_data: dict, gen_freq: int) -> dict:
    mls_per_sample = 1000 // gen_freq
    samples = request_data.get('data')
    sensor = request_data.get('sensor')
    sitting = request_data.get('sitting')
    device_id = request_data.get('device_id')
    
    change_timestamp = request_data.get('change_timestamp')
    last_timestamp = round_to_multiple(
        request_data.get('timestamp'), mls_per_sample)
    before_first_timestamp = last_timestamp - (len(samples) * mls_per_sample)

    if sensor == 'acc':
        data = acc_data
        other_sensor = 'gyro'
        other_data = gyro_data
    elif sensor == 'gyro':
        data = gyro_data
        other_sensor = 'acc'
        other_data = acc_data
    
    # if the recieved_data timestamps and local_data timestamps are overlaping
    # then shift the received data timestamps to the end
    shift = max(max(data, default=0) - before_first_timestamp, 0)
    data |= {
        row_timestamp: {
            'timestamp': row_timestamp,
            'sitting': sitting if row_timestamp >= change_timestamp else abs(
            sitting - 1),
            'device_id': device_id,
            f'{sensor}_x': sample.get('data_x'),
            f'{sensor}_y': sample.get('data_y'),
            f'{sensor}_z': sample.get('data_z')
        }
        for row_timestamp, sample in zip(range(last_timestamp + shift, before_first_timestamp + shift, -mls_per_sample), samples)
    }

    last_record_to_send = min(max(acc_data, default=0), max(gyro_data, default=0))
    data_to_send = {
        k: other_data.get(k, empty_record(other_sensor)) | v
        for k, v in data.items() if k <= last_record_to_send
    }
    for k in data_to_send:
        data.pop(k, None)
        other_data.pop(k, None)

    return data_to_send
    

def send_data_periodically(acc: Sensor, gyro: Sensor, acc_data: dict, gyro_data: dict, gen_freq: int):
    acc.start()
    gyro.start()

    for _ in range(10):
        time.sleep(50 // gen_freq)
        acc_request = get_data_from(
            sensor=acc,
            sitting=1,
            gen_freq=gen_freq
        )
        save_json(receive_data(
            request_data=acc_request,
            acc_data=acc_data,
            gyro_data=gyro_data,
            gen_freq=gen_freq
        ))

        time.sleep(50 // gen_freq)
        gyro_request = get_data_from(
            sensor=gyro,
            sitting=1,
            gen_freq=gen_freq
        )
        save_json(receive_data(
            request_data=gyro_request,
            acc_data=acc_data,
            gyro_data=gyro_data,
            gen_freq=gen_freq
        ))

    acc.stop()
    gyro.stop()


def main():
    acc_data = {}
    gyro_data = {}

    frequency = 100  # metawear sensors have 100
    acc = Sensor('acc', frequency)
    gyro = Sensor('gyro', frequency)

    try:
        send_data_periodically(acc, gyro, acc_data,
                               gyro_data, frequency)
    except Exception as e:
        print(e)
        acc.stop()
        gyro.stop()
    except KeyboardInterrupt:
        acc.stop()
        gyro.stop()


if __name__ == '__main__':
    main()
