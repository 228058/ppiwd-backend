import datetime
import os
import uuid
import numpy as np

import uvicorn
from azure.cosmos import CosmosClient, PartitionKey, ContainerProxy
from azure.cosmos.exceptions import CosmosResourceExistsError
from fastapi import FastAPI, BackgroundTasks

from pydantic import BaseModel, validator, ValidationError
from ppiwd_machine_learning.api import SittingOrNotOracle

import alert
from data_processing import estimate_timestamps, stick_measures
from test import receive_data

app = FastAPI()

# for local testing
os.environ[
    "cosmos_db_primary_key"] = '1abHR01jkVkT19r753pp5GrLsaGZiKHFnbYTa1zg7lSFgR5dvkyfI6tckYXuNljGRJUqQNJ2m3EkI2kk7avg5Q=='
os.environ["cosmos_db_endpoint"] = 'https://ppipwd-pwr-cosmosdb.documents.azure.com:443/'

endpoint = os.environ["cosmos_db_endpoint"]
key = os.environ["cosmos_db_primary_key"]

client = CosmosClient(endpoint, key)

database = client.get_database_client("ppiwd-sensors-data")


def ensure_container_exist(name) -> ContainerProxy:
    return database.create_container_if_not_exists(name, partition_key=PartitionKey(path="/timestamp"))


sensor_container = ensure_container_exist("data")
alert_container = ensure_container_exist("alert")
results_container = ensure_container_exist("results")

oracle = SittingOrNotOracle()

acc_data = {}
gyro_data = {}


class Data(BaseModel):
    data_x: float
    data_y: float
    data_z: float


class DoubleSensorData(BaseModel):
    sitting: int
    timestamp: int
    device_id: str
    acc_x: float
    acc_y: float
    acc_z: float
    gyro_x: float
    gyro_y: float
    gyro_z: float

    @validator('sitting')
    def sitting_validator(cls, value):
        if value not in [0, 1]:
            raise ValidationError('sitting must be 0 or 1')
        return value

class DataForPrediction(BaseModel):
    acc_x: float
    acc_y: float
    acc_z: float
    gyro_x: float
    gyro_y: float
    gyro_z: float
    device_id: str
    timestamp: int

class DataForPredictionArray(BaseModel):
    data: list[DataForPrediction]

class DoubleSensorDataArray(BaseModel):
    data: list[DoubleSensorData]


class SingleSensorData(BaseModel):
    """
    Sensor musi mieć wartośći "acc" albo "gyro". Sitting może być równe o albo 1
    """
    data: list[Data]
    timestamp: int
    sensor: str
    sitting: int
    change_timestamp: int
    device_id: str

    @validator('sensor')
    def sensor_validator(cls, value):
        if value not in ['acc', 'gyro']:
            raise ValidationError('sensor must be "acc" or "gyro"')
        return value

    @validator('sitting')
    def sitting_validator(cls, value):
        if value not in [0, 1]:
            raise ValidationError('sitting must be 0 or 1')
        return value


@app.get("/")
async def read_items():
    return list(sensor_container.read_all_items(max_item_count=10))


@app.post("/data/")
async def create_double_sensor_data(data: DoubleSensorData):
    """
    Odbiera dane z akcelerometru i żyroskopu razem (przestarzały endpoint, używać create_single_sensor_data)
    """
    data_dict = data.dict()
    data_id = str(uuid.uuid4())
    data_dict.update({"id": data_id})
    sensor_container.create_item(body=data_dict)
    return data_dict


@app.post("/data/array/")
async def create_double_sensor_data_array(data: list[DoubleSensorData]):
    for data_item in data:
        data_dict = data_item.dict()
        device_id = data_dict.get('device_id')
        timestamp = data_dict.get('timestamp')
        try:
            sensor_container.create_item(body=data_dict | {'id': f'{device_id}-{timestamp}'})
        except CosmosResourceExistsError:
            print('you\'ve tried to duplicate data bitch')
    return data


@app.get("/count")
async def get_number_of_sensor_records():
    result = list(
        sensor_container.query_items(query="SELECT VALUE COUNT(1) FROM data", enable_cross_partition_query=True))

    return result[0]


@app.get("/count_by_device")
async def get_number_of_sensor_records_by_device():
    return list(sensor_container.query_items(
        query="SELECT VALUE root FROM (SELECT COUNT(1), d.device_id FROM data d GROUP BY d.device_id) as root",
        enable_cross_partition_query=True))


'''
@app.post("/data/single-sensor/")
async def create_single_sensor_data(data: SingleSensorData):
    data_dict = data.dict()
    data_id = str(uuid.uuid4())
    data_dict.update({"id": data_id})
    container.create_item(body=data_dict)
    return data_dict
'''


@app.post("/data/single-sensor/")
async def create_single_sensor_data(data: SingleSensorData):
    """
    Odbiera dane z sensora (akcelerometru bądź żyroskopu). Możliwe wartości 'sensor': "acc", "gyro"
    """
    data_dict = data.dict()

    print("Received measurement at ", datetime.datetime.now().time())

    if data.device_id not in acc_data:
        acc_data[data.device_id] = {}

    if data.device_id not in gyro_data:
        gyro_data[data.device_id] = {}

    data_to_send = receive_data(data_dict, acc_data[data.device_id], gyro_data[data.device_id], 100)
    for k, v in data_to_send.items():
        try:
            v['id'] = f'{data.device_id}-{k}'
            sensor_container.create_item(body=v)
        except CosmosResourceExistsError:
            print('you\'ve tried to duplicate data bitch')

    # if data_dict.get('sensor') == 'acc':
    #     try_save_sensor_record(acc_data[data.device_id], gyro_data[data.device_id], data_dict)
    # elif data_dict.get('sensor') == 'gyro':
    #     try_save_sensor_record(gyro_data[data.device_id], acc_data[data.device_id], data_dict)

    return data


def try_save_sensor_record(primary_storage, secondary_storage, data_dict):
    """
    Próbuje dopasować jeden typ danych do drugiego (acc do gyro lub na odwrót)
    :param primary_storage: lista pomiarów typu który w przyszedł do endponta
    :param secondary_storage: lista pomiarów typu, do którego będziemy dopasowywać timestampy
    :param data_dict: dane które przyszły do endpointa
    :return:
    """
    db_record = {}
    primary_storage.append(estimate_timestamps(data_dict))
    data_id = str(uuid.uuid4())
    if len(secondary_storage) != 0:
        db_record["data"] = stick_measures(primary_storage.pop(0), secondary_storage.pop(0))
        db_record["id"] = data_id
        sensor_container.create_item(body=db_record)


@app.post("/data/clear")
async def clear_database():
    """
    Usuwa wszystkie pomiary z bazy
    :return:
    """
    global sensor_container
    database.delete_container(sensor_container)
    sensor_container = ensure_container_exist("data")


@app.post("/alert/create")
async def create_alert(alert: alert.Alert):
    """
    Tworzy alert
    :return:
    """
    data_dict = alert.dict()
    data_id = str(uuid.uuid4())
    data_dict.update({"id": data_id})

    alert_container.create_item(body=data_dict)


class GetReportParameters(BaseModel):
    """
    Kontener na parametry dla endpointu get_alerts_report
    """
    device_id: str
    time_frames: list[alert.TimeFrame]


@app.post("/alert/get_report")
async def get_alerts_report(arguments: GetReportParameters):
    """
    Generuje raport na podstawie alertów dla podanego device_id i timestampóœ
    """
    periods = alert.generate_alert_report_for_all_periods(alert_container, arguments.time_frames, arguments.device_id)

    return periods


@app.get("/alert/get_all")
async def get_all_alerts(device_id: str):
    """
    Generuje raport na podstawie alertów dla podanego device_id i timestampóœ
    """

    return alert.get_all_alerts(alert_container, device_id)


def ml_task(data, timestamp, device_id):
    results = oracle.predict(data)

    result_dict = {}
    result_dict.update({"device_id": device_id})
    result_dict.update({"prediction_results": results.tolist()})
    result_dict.update({"timestamp": timestamp})

    results_container.create_item(body=result_dict | {'id': f'{str(uuid.uuid4())}-{timestamp}'})

@app.post("/run_ml_task")
async def run_ml_task(data: list[DataForPrediction]):

    device_id = data[0].dict().get('device_id')
    if len(data) == 1000:
        timestamp = data[-1].dict().get('timestamp')

        # data_for_ml = [[
        #     data.get('acc_x'),
        #     data.get('acc_y'),
        #     data.get('acc_z'),
        #     data.get('gyro_x'),
        #     data.get('gyro_y'),
        #     data.get('gyro_z'),
        # ] for data_item in data]

        data_for_ml = []
        for dictionary in data:
            single_package = []
            single_package.append(dictionary.dict().get('acc_x'))
            single_package.append(dictionary.dict().get('acc_y'))
            single_package.append(dictionary.dict().get('acc_z'))
            single_package.append(dictionary.dict().get('gyro_x'))
            single_package.append(dictionary.dict().get('gyro_y'))
            single_package.append(dictionary.dict().get('gyro_z'))
            data_for_ml.append(single_package)

        print(data_for_ml)
        data_for_ml = np.array(data_for_ml).reshape(1, 1000, 6)

        ml_task(data=data_for_ml, timestamp=timestamp, device_id=device_id)

        query = f"""SELECT TOP 1 * FROM results r WHERE r.device_id = \"{device_id}\" AND r.timestamp = {timestamp} order by r._ts desc"""
        
        return {"Sitting": str(list(
            results_container.query_items(
                query=query,
                enable_cross_partition_query=True))[0].get('prediction_results'))}
    return {}

@app.get("/prediction_results")
async def get_prediction_results():
    return list(results_container.read_all_items(max_item_count=10))


@app.get("/prediction_results/{device_id}")
async def get_prediction_result(device_id: str):
    query = f"""SELECT * FROM results r WHERE r.device_id = \"{device_id}\""""
    return list(
        results_container.query_items(
            query=query,
            enable_cross_partition_query=True))




# sum_periods_between_alerts(alert_container, 0, 1000, "string")

if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.2", port=8000)
