from azure.cosmos import ContainerProxy
from pydantic import BaseModel


# class Period:
#     length: int
#     sitting_value: int


class Alert(BaseModel):
    device_id: str  # id urządzenia które wygenerowało alert
    timestamp: int  # czas pojeawienia się alerta


class TimeFrame(BaseModel):
    start: int
    end: int


def get_alerts(data_source: ContainerProxy, time_start: int, time_end: int, device_id: str):
    query = f"""SELECT * FROM alert a WHERE a.timestamp >= {time_start} AND a.timestamp <= {time_end} AND a.device_id = \"{device_id}\" ORDER BY a.timestamp"""
    print(query)
    return list(
        data_source.query_items(
            query=query,
            enable_cross_partition_query=True))


def count_alerts(data_source: ContainerProxy, time_start: int, time_end: int, device_id: str):
    query = f"""SELECT VALUE root FROM (SELECT COUNT(1) FROM alert a WHERE a.timestamp >= {time_start} AND a.timestamp <= {time_end} AND a.device_id = \"{device_id}\") as root"""
    print(query)
    return list(
        data_source.query_items(
            query=query,
            enable_cross_partition_query=True))[0]["$1"]


def get_all_alerts(data_source: ContainerProxy, device_id: str):
    query = f"""SELECT * FROM alert a WHERE a.device_id = \"{device_id}\" ORDER BY a.timestamp"""
    print(query)
    return list(data_source.query_items(
        query=query,
        enable_cross_partition_query=True))


class ReportResultForFrame:
    start: int
    end: int
    alerts_count: int


def generate_alert_report_for_all_periods(data_source: ContainerProxy, time_frames: list[TimeFrame], device_id: str):
    result = dict()
    result["device_id"] = device_id
    result["time_frames"] = list()

    for frame in time_frames:
        entry = ReportResultForFrame()
        entry.start = frame.start
        entry.end = frame.end
        entry.alerts_count = count_alerts(data_source, frame.start, frame.end, device_id)
        result["time_frames"].append(entry)

    return result


# def sum_periods_between_alerts(data_source: ContainerProxy, time_start: int, time_end: int, device_id: str):
#     alerts = get_alerts(data_source, time_start, time_end, device_id)
#
#     periods = []  # typ: Period
#     last_alert_timestamp = -1
#     last_alert_sitting_state = -1  # docelowo: 0 albo 1
#
#     for alert in alerts:
#         if last_alert_sitting_state == -1:
#             # current_standing_value = alert.sitting
#             last_alert_timestamp = alert["timestamp"]
#             last_alert_sitting_state = alert["sitting"]
#             continue
#
#         if last_alert_sitting_state == alert["sitting"]:
#             # wartość sitting nie zmieniła się. Pomijamy zakładając że to błąd
#             continue
#
#         period = Period()
#         period.length = alert["timestamp"] - last_alert_timestamp
#         period.sitting_value = last_alert_sitting_state
#
#         periods.append(period)
#
#         last_alert_timestamp = alert["timestamp"]
#         last_alert_sitting_state = alert["sitting"]
#
#     return periods
