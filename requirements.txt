anyio==3.5.0
asgiref==3.5.0
azure-core==1.23.1
azure-cosmos==4.2.0
certifi==2021.10.8
charset-normalizer==2.0.12
click==8.0.4
colorama==0.4.4
fastapi==0.75.0
h11==0.13.0
httptools==0.4.0
idna==3.3
importlib-metadata==4.11.3
joblib==1.1.0
numpy==1.21.6
pydantic==1.9.0
python-dotenv==0.19.2
PyYAML==6.0
requests==2.27.1
scikit-learn==1.1.1
scipy==1.8.1
six==1.16.0
sklearn==0.0
sniffio==1.2.0
starlette==0.17.1
threadpoolctl==3.1.0
typing_extensions==4.1.1
urllib3==1.26.9
uvicorn==0.17.6
watchgod==0.8
websockets==10.2
zipp==3.7.0
