from pydantic import BaseModel, validator, ValidationError
import time

class Data(BaseModel):
    data_x: float
    data_y: float
    data_z: float

class SingleSensorData(BaseModel):
    data: list[Data]
    timestamp: int
    sensor: str
    sitting: int
    change_timestamp: int

    @validator('sensor')
    def sensor_validator(cls, value):
        if not value in ['acc', 'gyro']:
            raise ValidationError('sensor must be "acc" or "gyro"')
        return value

    @validator('sitting')
    def sitting_validator(cls, value):
        if not value in [0, 1]:
            raise ValidationError('sitting must be 0 or 1')
        return value

timestamp = int(round(time.time() * 1000))

d_acc = {
    'data': [
        { 
            'data_x': 11.2,
            'data_y': 12.3,
            'data_z': 155,
        },
         { 
            'data_x': 156.3,
            'data_y': 1.23,
            'data_z': 1.55,
        },
    ],
    'timestamp': timestamp,
    'sensor': 'acc',
    'sitting': 0,
    'change_timestamp': timestamp + 12000
}

d_gyro = {
    'data': [
        { 
            'data_x': 12.2,
            'data_y': 92.3,
            'data_z': 1.155,
        },
         { 
            'data_x': 1.3,
            'data_y': 166.23,
            'data_z': 123.55,
        },
        { 
            'data_x': 156.3,
            'data_y': 1.23,
            'data_z': 1.55,
        },
        { 
            'data_x': 156.3,
            'data_y': 1.23,
            'data_z': 1.55,
        },

    ],
    'timestamp': timestamp + 11000,
    'sensor': 'gyro',
    'sitting': 0,
    'change_timestamp': timestamp + 12000
}

acc_data = list()
gyro_data = list()

def is_timestamp_similar(first_timestamp, second_timestamp, scope=10000):
    if first_timestamp == second_timestamp:
        return True
    elif first_timestamp > second_timestamp and first_timestamp - scope > second_timestamp:
        return False
    elif first_timestamp > second_timestamp and first_timestamp - scope <= second_timestamp:
        return True
    elif first_timestamp < second_timestamp and first_timestamp + scope < second_timestamp:
        return False
    elif first_timestamp < second_timestamp and first_timestamp + scope >= second_timestamp:
        return True

def estimate_timestamps(data: SingleSensorData):
    timestamp = data.get('timestamp')
    measures = data.get('data')
    sensor = data.get('sensor')
    sitting = data.get('sitting')
    change_timestamp = data.get('change_timestamp')
    prepared_data = list()
    while measures:
        measure = measures.pop(-1)
        prepared_data.insert(0, {'timestamp': timestamp, 'sensor': sensor, 'data': measure, 
                                'sitting': sitting, 'change_timestamp': change_timestamp})
        timestamp -= 10

    return prepared_data

def find_similar(measure, measure_array):
    pair_of_measures = list()
    for array_measure in measure_array:
        if is_timestamp_similar(measure.get('timestamp'), array_measure.get('timestamp')):
            pair_of_measures = [measure, array_measure]
            break
    return pair_of_measures
    

def stick_measures(first_sensor, second_sensor):
    sticked_data = list()
    first_data = first_sensor[:]
    second_data = second_sensor[:]

    
    for first_sensor_measure in first_sensor:
        if first_data and second_data:
            pair_of_measures = find_similar(first_sensor_measure, second_data)
            if pair_of_measures:
                sticked_data.append(pair_of_measures)
                first_data.remove(pair_of_measures[0])
                second_data.remove(pair_of_measures[1])
    
    if len(first_data) != 0:
        for measure in first_data:
            sticked_data.append([measure])
    if len(second_data) != 0:
        for measure in second_data:
            sticked_data.append([measure])

    return sticked_data

    

acc_data.append(estimate_timestamps(d_acc))
gyro_data.append(estimate_timestamps(d_gyro))
# print(stick_measures(acc_data[0], gyro_data[0]))

